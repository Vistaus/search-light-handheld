# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the searchlight.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: searchlight.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-26 22:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:72 searchlight.desktop.in.h:1
msgid "Search Light Handheld"
msgstr ""

#: ../qml/Main.qml:435
msgid "Game over"
msgstr ""

#: ../qml/Main.qml:440
msgid "Score %1"
msgstr ""

#: ../qml/Main.qml:445
msgid "Restart the game"
msgstr ""

#: searchlight.desktop.in.h:2
msgid "retro;lcd game;scape;"
msgstr ""
